# S3 Replication Helm Chart

This Helm chart deploys Kubernetes resources for S3 replication tasks. It creates a CronJob that can perform backup and archiving operations between S3 buckets with configurable schedules and resource constraints.

## Features

- Configurable CronJob for periodic S3 replication tasks
- Support for both backup and archive operations
- Prometheus monitoring integration
- Flexible resource management and scheduling
- Registry credentials management
- Comprehensive environment variable configuration

## Installation

Install the chart using Helm:

```bash
helm install s3-replicator ./s3-replication-chart
```

## Configuration

### Image Configuration

```yaml
image:
  repository: registry.gitlab.com/ccsolutions.io/open-source/docker/s3-replicator
  pullPolicy: IfNotPresent
  tag: "1.0.6"
```

### Registry Credentials

```yaml
registryCredentials:
  name: gitlab-registry
  registry: registry.gitlab.com
  username: ""
  password: ""
```

### Scheduling

```yaml
timeZone: "Europe/Berlin"
schedule: "*/5 * * * *"  # Runs every 5 minutes
```

### Operation Mode

```yaml
args: "backup"  # Options: "backup" or "archive"
```

### Environment Variables

Required environment variables for configuration:

```yaml
env:
  SOURCE_ACCESS_KEY_ID: "your_source_access_key_id"
  SOURCE_SECRET_ACCESS_KEY: "your_source_secret_access_key"
  SOURCE_REGION: "your_source_region"
  SOURCE_ENDPOINT_URL: "https://s3.your_source_region.amazonaws.com"
  SOURCE_BUCKET: "your-source-bucket-name"
  TARGET_ACCESS_KEY_ID: "your_target_access_key_id"
  TARGET_SECRET_ACCESS_KEY: "your_target_secret_access_key"
  TARGET_REGION: "your_target_region"
  TARGET_ENDPOINT_URL: "https://s3.your_target_region.amazonaws.com"
  TARGET_BUCKET: "your-target-bucket-name"
  IS_CLEAN_UP: "true"
  BACKUP_RETENTION_DAYS: "30"
  ARCHIVE_RETENTION_WEEKS: "4"
  LOGGING_LEVEL: "info"
  MAX_RETRIEVE: "5"
  DEFAULT_PERIOD: "YESTERDAY_TODAY"
```

### CronJob Configuration

```yaml
replicatorcronJob:
  name: s3-replicator
  concurrencyPolicy: "Forbid"
  cronJobHistory:
    failedJobsHistoryLimit: 1
    successfulJobsHistoryLimit: 1
  resources:
    limits:
      cpu: 500m
      memory: 1024Mi
    requests:
      cpu: 100m
      memory: 128Mi
  restartPolicy: OnFailure
  backoffLimit: 3
  nodeSelector: {}
  tolerations: []
  affinity: {}
  securityContext: {}
```

### Prometheus Monitoring

```yaml
metrics:
  prometheusRules:
    enabled: false
    additionalLabels: {}
```

When enabled, the chart creates PrometheusRules for monitoring:
- Job failure alerts
- CronJob suspension alerts

## Alerts

### Job Failure Alert
Triggers when a replication job fails to complete successfully.

### CronJob Suspended Alert
Triggers when the replication CronJob is suspended.

## Custom Labels and Annotations

The chart supports adding custom labels and annotations at various levels:

```yaml
labels: {}      # Global labels
annotations: {} # Global annotations

replicatorcronJob:
  labels: {}      # CronJob specific labels
  annotations: {} # CronJob specific annotations
```

## Security Context

You can configure security context settings for the pods:

```yaml
replicatorcronJob:
  securityContext:
    capabilities:
      drop:
        - ALL
    readOnlyRootFilesystem: true
    runAsNonRoot: true
    runAsUser: 1000
```

## Uninstalling

To remove the chart and all associated resources:

```bash
helm uninstall s3-replicator
```

## Version Information

- Chart Version: 2.0.3
- App Version: v2.0.3

## Notes

- The chart creates Kubernetes secrets for storing sensitive information
- All times are interpreted in the configured timezone
- Failed jobs are retained according to the `failedJobsHistoryLimit` setting
- Successful jobs are retained according to the `successfulJobsHistoryLimit` setting
- The chart supports both backup and archive operations through the `args` parameter