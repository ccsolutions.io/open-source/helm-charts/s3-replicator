Thank you for installing {{ .Chart.Name }}.

Remember, the actual running of jobs is dependent on your CronJob's schedule and successful scheduling by Kubernetes.

For more information on managing CronJobs and their jobs, visit the Kubernetes documentation at:
https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/
